
[![pipeline status](https://gitlab.com/holtjp/todo/badges/master/pipeline.svg)](https://gitlab.com/holtjp/todo/commits/master) [![coverage report](https://gitlab.com/holtjp/todo/badges/master/coverage.svg)](https://gitlab.com/holtjp/todo/commits/master)

# todo

A _very_ simply Python package for managing a todo list :)