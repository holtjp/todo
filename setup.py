from setuptools import setup


setup(
    name='todo',
    version='0.1',
    description='A basic application for managing a TODO list.',
    author='James Holt',
    packages=['todo']
)

