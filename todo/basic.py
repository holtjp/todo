"""
Basic todo commands required for all modules.
"""


TODOS = dict()


def create_todo(title, descr):
    """
    Add a TODO task to the dictionary.
    """
    id = len(TODOS.keys()) + 1
    
    TODOS[id] = dict(title=title, description=descr)
    
    return TODOS
    
    
def delete_todo(title):
    """
    Remove a todo from the dictionary.
    """
    key = int()
    for k, v in TODOS.items():
        if v['title'] == title:
            key = k
    del TODOS[key]
    
    return TODOS

