"""
Testing basic functions
"""


from .. import basic


def test_create_todo():
    result = {
        1: {
            'title': 'Groceries',
            'description': 'Buy milk and eggs'
        }
    }
    assert basic.create_todo(title='Groceries', descr='Buy milk and eggs') == result
    

def test_delete_todo():
    assert basic.delete_todo(title='Groceries') == dict()
